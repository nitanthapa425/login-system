import { HttpStatus, baseUrl } from "../config/constant.js";
import successResponse from "../helper/successResponse.js";
import { User } from "../schema/model.js";
import expressAsyncHandler from "express-async-handler";
import { sendMail } from "../utils/sendMail.js";
import { comparePassword, hashPassword } from "../utils/hashing.js";
import { generateToken, verifyToken } from "../utils/token.js";
import { Token } from "../schema/model.js";

export let createUser = expressAsyncHandler(async (req, res, next) => {
  let data = req.body;
  data.isVerify = false;
  data.isDeactivate = false;
  let email = data.email;
  let user = await User.findOne({ email: email });

  if (user) {
    let error = new Error("Duplicate email.");
    error.statusCode = 409;
    throw error;
  } else {
    let _hashPassword = await hashPassword(data.password);
    data.password = _hashPassword;
    let result = await User.create(req.body);
    delete result._doc.password;
    let infoObj = {
      id: result._id,
      role: result.role,
    };
    let expireInfo = {
      expiresIn: "1d",
    };
    let token = await generateToken(infoObj, expireInfo);
    await Token.create({ token });
    let link = `${baseUrl}/verify-email?token=${token}`;
    await sendMail({
      from: '"Nitan Thapa" <uniquekc425@gmail.com>',
      to: [data.email],
      subject: "Email verification",
      html: `<h1>
    Verify Email
    <a href = "${link}">Click to verify</a>
    <h1>`,
    });

    successResponse(
      res,
      HttpStatus.CREATED,
      "User created successfully",
      result
    );
  }
});

//localhost:8000/user/email-verify?id=1234234
export let verifyEmail = expressAsyncHandler(async (req, res, next) => {
  let id = req.info.id;
  let tokenId = req.token.tokenId;
  let result = await User.findByIdAndUpdate(
    id,
    { isVerify: true },
    { new: true }
  );
  delete result._doc.password;
  await Token.findByIdAndDelete(tokenId);
  successResponse(
    res,
    HttpStatus.CREATED,
    "Email verified successfully.",
    result
  );
});

export let loginUser = expressAsyncHandler(async (req, res, next) => {
  let email = req.body.email;
  let password = req.body.password;
  let data = await User.findOne({ email: email }); //if not present null
  if (data.isDeactivate) {
    await User.findByIdAndUpdate(data._id, { isDeactivate: false }); //isDeactivate false when logged in
  }

  if (!data) {
    let error = new Error("Credential doesn't match");
    error.statusCode = 401;
    throw error;
  } else {
    let isValidPassword = await comparePassword(password, data.password);
    if (!isValidPassword) {
      let error = new Error("Credential doesn't match");
      error.statusCode = 401;
      throw error;
    } else {
      if (!data.isVerify) {
        let error = new Error("Please Verify Your Account First.");
        error.statusCode = 401;
        throw error;
      } else {
        let infoObj = {
          id: data._id,
          role: data.role,
        };
        let expireInfo = {
          expiresIn: "365d",
        };
        let token = await generateToken(infoObj, expireInfo);
        console.log(token);
        await Token.create({ token });
        successResponse(res, HttpStatus.CREATED, "Login Successfully", token);
      }
    }
    // console.log("isValidPassword", isValidPassword);
  }
});

export let myProfile = expressAsyncHandler(async (req, res, next) => {
  let id = req.info.id;
  let result = await User.findById(id);
  successResponse(res, HttpStatus.OK, "My-profile read successfully", result);
});

export let logout = expressAsyncHandler(async (req, res, next) => {
  let tokenId = req.token.tokenId;
  let result = await Token.findByIdAndDelete(tokenId);
  successResponse(res, HttpStatus.OK, "logout successfully");
});

export let updateMyProfile = expressAsyncHandler(async (req, res, next) => {
  let id = req.info.id;
  let data = req.body;
  delete data.email;
  delete data.password;
  delete data.isVerify;
  // console.log(data);
  let result = await User.findByIdAndUpdate(id, data, { new: true });
  delete result._doc.password;
  successResponse(res, HttpStatus.OK, "update successfully", result);
});

export let updatePassword = expressAsyncHandler(async (req, res, next) => {
  let id = req.info.id;
  let tokenId = req.token.tokenId;

  let _hashPassword = await hashPassword(req.body.password);
  let data = { password: _hashPassword };
  let result = await User.findByIdAndUpdate(id, data, { new: true });
  delete result._doc.password;

  await Token.findByIdAndDelete(tokenId);
  successResponse(res, HttpStatus.OK, "updated password successfully", result);
});

export let deactivate = expressAsyncHandler(async (req, res, next) => {
  let id = req.info.id;
  let user = await User.findByIdAndUpdate(
    id,
    { isDeactivate: true },
    { new: true }
  );
  successResponse(res, HttpStatus.OK, "Account deactivated successfully", user);
});

//only verified user can create account (alternative to register)

// export let createUser = expressAsyncHandler(async (req, res, next) => {
//   let data = req.body;
//   let _hashPassword = await hashPassword(data.password);
//   data.password = _hashPassword;
//   // let result = await User.create(req.body);
//   // delete result._doc.password;
//   await sendMail({
//     from: '"Nitan Thapa" <uniquekc425@gmail.com>',
//     to: [data.email],
//     subject: "Email verification",
//     html: `<h1>
//     Verify Email
//     ${data}
//     <h1>`,
//   });

//   successResponse(res, HttpStatus.CREATED, "User created successfully");
// });

// export let verifyEmail = expressAsyncHandler(async (req, res, next) => {
//   let data = req.body;

//   let result = await User.create(data);
//   delete result._doc.password;
//   successResponse(
//     res,
//     HttpStatus.CREATED,
//     "Email verified successfully.",
//     result
//   );
// });

export let readUserDetails = expressAsyncHandler(async (req, res, next) => {
  let result = await User.findById(req.params.id);
  successResponse(res, HttpStatus.OK, "Read User details successfully", result);
});

//get all
//update details (id)
//delete (id)

export let readAllUser = expressAsyncHandler(async (req, res, next) => {
  try {
    let result = await User.find({});

    successResponse(res, HttpStatus.OK, "Read User  successfully", result);
  } catch (error) {
    errorResponse(res, HttpStatus.BAD_REQUEST, error.message);
  }
});

//Super admin can delete user.
//admin cannot delete user

export let deleteUser = expressAsyncHandler(async (req, res, next) => {
  try {
    let result = await User.findByIdAndDelete(req.params.id);
    successResponse(res, HttpStatus.OK, "Delete User  successfully.", result);
  } catch (error) {
    error.statusCode = HttpStatus.BAD_REQUEST;
    next(error);
  }
});

export let updateUser = expressAsyncHandler(async (req, res, next) => {
  try {
    let result = await User.findByIdAndUpdate(req.params.id, req.body);
    successResponse(
      res,
      HttpStatus.CREATED,
      "Update User  successfully.",
      result
    );
  } catch (error) {
    error.statusCode = HttpStatus.BAD_REQUEST;
    next(error);
  }
});

export let forgetPassword = expressAsyncHandler(async (req, res, next) => {
  let email = req.body.email;

  let data = await User.findOne({ email });

  let infoObj = {
    id: data._id,
    role: data.role,
  };
  let expireInfo = {
    expiresIn: "1d",
  };

  let token = await generateToken(infoObj, expireInfo);

  await Token.create({ token }); //saved to database

  await sendMail({
    from: '"Nitan Thapa" <uniquekc425@gmail.com>',
    to: [data.email],
    subject: "Email verification",
    html: `<h1>
    Verify Email
    ${token}
    <h1>`,
  });

  successResponse(res, HttpStatus.OK, "Mail sent successfully");
});

export let resetPassword = expressAsyncHandler(async (req, res, next) => {
  console.log(req.info);
  console.log(req.token);
  let id = req.info.id;
  let tokenid = req.token.tokenId;

  let newpassword = await hashPassword(req.body.password);

  await User.findByIdAndUpdate(id, newpassword, { new: true });
  await Token.findByIdAndDelete(tokenid);

  //  await Token.create({token})      //saved to database

  successResponse(res, HttpStatus.OK, "password updated successfully");
});
