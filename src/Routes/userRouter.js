import { Router } from "express";
import {
  createUser,
  deactivate,
  deleteUser,
  forgetPassword,
  loginUser,
  logout,
  myProfile,
  readAllUser,
  readUserDetails,
  resetPassword,
  updateMyProfile,
  updatePassword,
  updateUser,
  verifyEmail,
} from "../controller/userController.js";

import isAuthenticated from "../middleware/isAuthenticated.js";
import isAuthorized from "../middleware/isAuthorized.js";
// import isAuthorized from "../middleware/isAuthorized.js";

let userRouter = Router();

//localhost:8000/users/verify-email

userRouter.route("/").post(createUser).get(readAllUser);

userRouter.route("/verify-email").patch(isAuthenticated, verifyEmail);

userRouter.route("/login").post(loginUser);

userRouter.route("/my-profile").get(isAuthenticated, myProfile);

userRouter.route("/logout").delete(isAuthenticated, logout);

userRouter.route("/update-my-profile").patch(isAuthenticated, updateMyProfile);

userRouter.route("/forget-password").post(forgetPassword);
userRouter.route("/reset-password").post(isAuthenticated, resetPassword);
userRouter.route("/update-password").patch(isAuthenticated, updatePassword);
userRouter.route("/deactivate").patch(isAuthenticated, deactivate);

userRouter
  .route("/:id")
  .get(readUserDetails)
  .patch(updateUser)
  .delete(isAuthenticated, isAuthorized(["superAdmin", "admin"]), deleteUser);

export default userRouter;
